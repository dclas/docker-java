# Java Dockerfile
# Based on dockerfile/java

# Using phusion as the base image.
FROM phusion/baseimage

MAINTAINER David Laskowski

# Define default command.
CMD ["bash"]
