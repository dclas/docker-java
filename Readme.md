#Docker image for Oracle Java#

This project defines a docker container that includes Oracle's Java JDK.

##Branches##

Choose the appropriate branch for the version of Java to be included:

 - 1.7 branch for Java 7
 - 1.7 branch for Java 8

You can also choose a specific version of java by checking out by its corresponding tag. Note that the master branch does not contain java -
it is only a pull of phusion/baseimage.

##Prerequistes##

A working installation of [Docker](http://www.docker.com) (or boot2docker) is required to build this container.

##Usage##

To build the image locally, use the following command to build the Docker container. Alternatively, you can use the autobuild project in the hub
repository.

````
git checkout 1.x
docker build -t dclas/java:1.x .
docker run -it dclas/java:1.x
````
